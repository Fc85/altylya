#  This script allows listening to UDP request on port 15555
#
#  V1.1 can receive keepAlive or Data, and display it on the console
#  Run this program and wait for incomming UDP packets

import sys
import socket
import json
import base64
import methodebdd
import methodelecture

# OPENING SOCKET ON PORT 15555
socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
socket.bind(('', 15555))

cur_version = sys.version_info # base64.decode differs from python 2.7 to 3.x

print("Gateway listener starts\n")
data = bytearray(480)

while True: # NEVER STOP
    
    nbytes,addr = socket.recvfrom_into(data, 480)
    message=methodelecture.lecture(socket,data,nbytes,addr,cur_version)
    if not(message ==  " "):
        syncro = methodelecture.keepalive(message)
        if(syncro == "ok") :
            print("Message decoder : "+str(message))
            capteur = bytes.fromhex(message).decode()
            print("Le message envoyé est : " + capteur)
            valeurs = methodebdd.parse(capteur)
            print(valeurs)
            nom=valeurs[0]
            print("idbat:",nom)
                
            valeurs = methodebdd.voirseuil(nom,valeurs)
            
            methodebdd.envoibdd(valeurs)

        else :
            print(message)
        #print("JSON : "+data2+"\n")
