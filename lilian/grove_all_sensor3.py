import sys
path="/home/pi/Desktop/GrovePi/Software/Python"
sys.path.append(path)
import time
import grovepi
import atexit
import math
import testdust
import smbus
import RPi.GPIO as GPIO

# CHERHCE (AMMONIAQUE)
rev = GPIO.RPI_REVISION
if rev == 2 or rev == 3:
    bus = smbus.SMBus(1)
else:
    bus = smbus.SMBus(0)
# Adresse I2C du CAPTEUR AMMONIAC
address = 0x19

def readNumber(regaddr):
    #number = bus.read_byte(address)
    number = bus.read_i2c_block_data(address, regaddr)
    dta = number[0]
    dta <<= 8
    dta += number[1]
    return dta

#CAPTEUR TEMP&HUM SUR D4
sensor = 4
blue = 0
white = 1
#CAPTEUR POUSSIERE SUR D2
atexit.register(grovepi.dust_sensor_dis)

#print("Lecture des capteurs :")
grovepi.dust_sensor_en()

#INITIALISATION VALEUR TOTALCAPTEUR (MOYENNE)
htotal =0
ttotal =0
ptotal =0
atotal =0

#CREATION D'UNE BOUCLE (nrep, pour faire ensuite une moyenne)
i=0
while i<5:
    try:
        [new_val,lowpulseoccupancy] = grovepi.dustSensorRead()
        
        [temp,humidity] = grovepi.dht(sensor,white)
        
        while i <5 and i>1:
            #print("TEST : ", i-1)
            result = testdust.pcs_to_ugm3(lowpulseoccupancy)
            #print(result)
            an_0 = readNumber(1)
            time.sleep(1)
            a0_0 = readNumber(8)
            time.sleep(1)
            ratio0 = (float(an_0)/float(a0_0))*((1023.0-a0_0)/(1023.0-an_0))
            a = pow(ratio0, -1.67)/1.47
            #print(a)
            [temp,humidity] = grovepi.dht(sensor,white) 
            if math.isnan(temp) == False and math.isnan(humidity) == False:
                #time.sleep(30)
                temp ="t%.02f"%(temp)
                humidity = "h%.02f%%"%(humidity)
                #print(temp)
                #print(humidity)
                humidity = float(humidity[1:-1])
                temp = float(temp[1:])
                
                htotal += humidity
                ttotal += temp
                ptotal += result
                atotal += a
                
                time.sleep(30)
                
                i+=1
    except IOError:
        i=i
    i+=1

htotal=htotal/3
ttotal=ttotal/3
ptotal=ptotal/3
atotal=atotal/3
htotal= "%.3f" % htotal
ttotal= "%.3f" % ttotal
ptotal= "%.3f" % ptotal
atotal= "%.3f" % atotal

msgfin=("h"+str(htotal)+":t"+str(ttotal)+":p"+str(ptotal)+":a"+str(atotal)+":")
print(msgfin)


