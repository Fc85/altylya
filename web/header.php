<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" /> <!-- Configuration des caractère en français -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Activer la compatibilité -->
	<title>Supervision Avicole</title> <!-- Titre de l'onglet -->
	<link rel="stylesheet" type="text/css" media="screen" href="/style/header.css" /> <!-- Insertion de la page de style -->
	<?php include('scripts/connection_db.php'); ?> <!-- Connexion à la base de donnée -->
</head>

<header>
	<a class="titre" href="index.php"><p>Supervision avicole</p></a> <!-- Création d'un "bouton" qui ramène à l'index -->
</header>

</html>