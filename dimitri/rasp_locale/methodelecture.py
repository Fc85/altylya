import sys
import socket
import json
import base64
import methodebdd


def keepalive(message):
    i=0
    while i < len(message):
        if message[i] == " ":
            split = message[0:i]
            message = message[i+1:len(message)]
            if (split == "KeepAlive"):
                return "non"
        i+=1
    return "ok"


    
def lecture(socket,data,nbytes,addr,cur_version):

    if( nbytes > 0 ):
        macAddress=""
        for i in range(4, 8):
            macAddress += '{:02X}'.format(data[i]) + ":"
        macAddress += '{:02X}'.format(data[9]) # do not add ":" after last char
        if( nbytes == 12 ):
            #KEEPALIVE MESSAGE HAS A LENGTH = 12
            affiche=("KeepAlive received from : "+macAddress+"\n")
            return affiche
        else :
            #DATA TRANSMITTED    
            data2=data[12:nbytes].decode('utf8') #THIS IS THE DATA PART
            dico = json.loads(data2) #AFFECT TO JSON OBJECT
            list = dico["lwpk"] #CONVERT TO LIST FORMAT
            dic=list[0] #GET FIRST ITEM OF DICTIONNARY
            
                
            print("Data received from gateway : "+macAddress)
            print("Sent from device : "+dic['deui'])
            
            size = dic['size']
            secret = dic['data']
            message=""
            
            msgdec=base64.b64decode(secret, altchars=None) #DECODE MESSAGE
            
            if( cur_version >= (3, 0) ):
                for char in msgdec: #FOR EACH BYTE 
                    message += '{:02X}'.format(char)
            else:
                 for char in msgdec: #FOR EACH CHAR 
                    message += '{:02X}'.format(ord(char))
                 
            return message

    else :
        message=""
        return message
        
