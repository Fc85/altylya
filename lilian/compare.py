import sys
path="/home/pi/Desktop/GrovePi/Software/Python"
sys.path.append(path)
from grovepi import *
from parse import *

led = 3

pinMode(led,"OUTPUT")

def parseval(message):
    idbat = temperature = humidite = amoniac = poussiere =0
    i=0
    carac=":"

    while i < len(message):
        if message[i] == carac:
            split = message[0:i]
            message = message[i+1:len(message)]
            if split[0] == "i":
                nom = split[1:len(split)]
            if split[0] == "t":
                temperature = split[1:len(split)]
            elif split[0] == "h":
                humidite = split[1:len(split)]
            elif split[0] == "a":
                amoniac = split[1:len(split)]
            elif split[0] == "p":
                poussiere = split[1:len(split)]
            i=0
        else :i+=1
        valeurs = [temperature , humidite, amoniac , poussiere]

    return valeurs

def compareval(valeurs):
    
    val= parseval(valeurs)
    
    fichier =open("seuil","r")
    seuil = fichier.read()
    print (seuil)
    seuils=parse('tempmin:{}\ntempmax:{}\nhumi:{}\npouss:{}\nammo:{}',seuil)
    fichier.close()
    tempmin=seuils[0]
    tempmax=seuils[1]
    humi=seuils[2]
    pouss=seuils[3]
    ammo=seuils[4]
    
    if float(tempmin) < float(val[0]) and float(tempmax) > float(val[0]) and float(humi) > float(val[1]) and float(ammo) > float(val[2]) and float(pouss) > float(val[3]):
        seuil = 0
    else:
        seuil = 1
        
    if seuil == 1 :
        digitalWrite(led,1)
        print ("led on")
    else:
        digitalWrite(led,0)
        print ("led off")
        
    
    
    